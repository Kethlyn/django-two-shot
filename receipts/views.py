from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
# Create your views here.


@login_required
def receipt_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "Receipt": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_view(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def account_list_view(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "Account": account}
    return render(request, "accounts/list.html", context)


@login_required
def expensecategory_list_view(request):
    expensecategory = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "Expenses": expensecategory}
    return render(request, "expensecategory/list.html", context)


@login_required
def expense_category_create_view(request):
    if request.method == "POST":
        expenseform = ExpenseCategoryForm(request.POST)
        if expenseform.is_valid():
            expensecategory = expenseform.save(commit=False)
            expensecategory.owner = request.user
            expenseform.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "expensecategory/create.html", context)


@login_required
def account_create_view(request):
    if request.method == "POST":
        accountform = AccountForm(request.POST)
        if accountform.is_valid():
            accountform = accountform.save(commit=False)
            accountform.owner = request.user
            accountform.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
