from django.urls import path
from receipts.views import (
    receipt_view,
    create_view,
    account_list_view,
    expensecategory_list_view,
    expense_category_create_view,
    account_create_view,
)

urlpatterns = [
    path("", receipt_view, name="home"),
    path("create/", create_view, name="create_receipt"),
    path("accounts/", account_list_view, name="account_list"),
    path("categories/", expensecategory_list_view, name="category_list"),
    path(
        "categories/create/",
        expense_category_create_view,
        name="create_category",
    ),
    path(
        "accounts/create/",
        account_create_view,
        name="create_account",
    ),
]
