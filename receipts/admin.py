from django.contrib import admin
from receipts.models import Receipt
from receipts.models import Account


# Register your models here.
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor", "total",
        "tax", "date", "purchaser", "category", "account")


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "owner", "number")
