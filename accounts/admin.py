from django.contrib import admin
from receipts.models import ExpenseCategory


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "id")
